<?php

namespace Qubz\Tests;

use Qubz\Runner;

/**
 * Runner test cases.
 *
 * @author Artem Ivanov <aivanov@niklaus.ru>
 */
class RunnerTest extends \PHPUnit_Framework_TestCase
{
    public function testRun()
    {
        $runner = new Runner();

        $this->assertInstanceOf('Qubz\Runner', $runner);
    }
}
