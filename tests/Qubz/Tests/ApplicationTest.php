<?php

namespace Qubz\Tests;

use Qubz\Application;

/**
 * Application test cases.
 *
 * @author Artem Ivanov <aivanov@niklaus.ru>
 */
class ApplicationTest extends \PHPUnit_Framework_TestCase
{
    public function testApplicationRun()
    {
        $app = new Application();
        $runner = $app->run();
        
        $this->assertInstanceOf('Qubz\Application', $app);
        $this->assertInstanceOf('Qubz\Runner', $runner);
    }
}
