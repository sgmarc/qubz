<?php

namespace Qubz;

use Qubz\Runner;

/**
 * The Qubz framework class.
 *
 * @author Artem Ivanov <aivanov@niklaus.ru>
 */
class Application
{
    /**
     * Run application.
     */
    public function run()
    {
        $runner = new Runner();

        return $runner;
    }
}
