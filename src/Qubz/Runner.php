<?php

namespace Qubz;

/**
 * The Runner framework class.
 *
 * @author Artem Ivanov <aivanov@niklaus.ru>
 */
class Runner
{
    public function run()
    {
        return 'run';
    }
}
